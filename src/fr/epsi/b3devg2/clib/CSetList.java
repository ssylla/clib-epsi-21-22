package fr.epsi.b3devg2.clib;

import fr.epsi.b3devg2.clib.errors.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class CSetList implements ICSet {
	
	private List data;
	
	public CSetList() {
		data = new ArrayList<>();
	}
	
	public CSetList( List data ) {
		this.data = data;
	}
	
	@Override
	public void add( Object o ) {
		if (!data.contains( o )) {
			data.add( o );
		}
	}
	
	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}
	
	@Override
	public Object get( int index ) throws NotFoundException {
		return data.get( index );
	}
	
	@Override
	public boolean contains( Object o ) {
		return data.contains( o );
	}
	
	@Override
	public int getSize() {
		return data.size();
	}
	
	@Override
	public int getMaxSize() {
		return Integer.MAX_VALUE;
	}
	
	@Override
	public ICSet copy() {
		return new CSetList( data );
	}
	
	@Override
	public Object remove( int index ) {
		return data.remove( index );
	}
	
	@Override
	public void merge( ICSet set ) throws NotFoundException {
		for ( int i = 0, size = set.getSize(); i < size; ++i ) {
			add( set.get( i ) );
		}
	}
}
