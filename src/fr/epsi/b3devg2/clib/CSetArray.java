package fr.epsi.b3devg2.clib;

import fr.epsi.b3devg2.clib.errors.NotFoundException;

public class CSetArray implements ICSet {
	
	private Object[] data;
	private int currentIndex = -1;
	
	public CSetArray( int maxSize ) {
		if ( maxSize < 1 ) maxSize = Integer.MAX_VALUE;
		data = new Object[maxSize];
	}
	
	public CSetArray( Object[] data, int currentIndex ) {
		this.data = data;
		this.currentIndex = currentIndex;
	}
	
	@Override
	public void add( Object o ) {
		if ( currentIndex < data.length - 1 && !contains( o ) ) {
			data[++currentIndex] = o;
		}
	}
	
	@Override
	public boolean isEmpty() {
		return -1 == currentIndex;
	}
	
	@Override
	public Object get( int index ) throws NotFoundException {
		if ( index < 0 || index > currentIndex ) {
			throw new NotFoundException( "Item not found!", index, this );
		}
		return data[index];
	}
	
	@Override
	public boolean contains( Object o ) {
		boolean found = false;
		int i = 0;
		while ( i <= currentIndex && !found ) {
			found = data[i].equals( o );
			++i;
		}
		return found;
	}
	
	@Override
	public int getSize() {
		return currentIndex + 1;
	}
	
	@Override
	public int getMaxSize() {
		return data.length;
	}
	
	@Override
	public ICSet copy() {
		return new CSetArray( data, currentIndex );
	}
	
	@Override
	public Object remove( int index ) {
		Object o = null;
		if ( contains( index ) ) {
			o = data[index];
			for ( int i = index; i < currentIndex; ++i ) {
				data[i] = data[i + 1];
			}
			data[currentIndex--] = null;
		}
		return o;
	}
	
	@Override
	public void merge( ICSet set ) throws NotFoundException {
		
		for ( int i = 0, size = set.getSize(); i < size; ++i ) {
			add( set.get( i ) );
		}
	}
}
