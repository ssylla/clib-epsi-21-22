package fr.epsi.b3devg2.clib.errors;

import fr.epsi.b3devg2.clib.ICSet;

public class NotFoundException extends Exception {
	
	private int index;
	private ICSet set;
	
	public NotFoundException( String message, int index, ICSet set ) {
		super( message );
		this.index = index;
		this.set = set;
	}
	
	@Override
	public String getMessage() {
		return super.getMessage()+"\nElement non trouvé à l'index : "+index+
				"\nLa taille de cet ensemble est "+set.getSize();
	}
	
	public int getIndex() {
		return index;
	}
	
	public ICSet getSet() {
		return set;
	}
}
