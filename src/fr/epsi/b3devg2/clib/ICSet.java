package fr.epsi.b3devg2.clib;

import fr.epsi.b3devg2.clib.errors.NotFoundException;

public interface ICSet {
	
	void add(Object o);
	boolean isEmpty();
	Object get( int index ) throws NotFoundException;
	boolean contains(Object o);
	int getSize();
	int getMaxSize();
	
	ICSet copy();
	Object remove(int index);
	void merge(ICSet set) throws NotFoundException;
}
