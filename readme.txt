##### Exercice – Création de librairies #####

Ecrire la spécification d’un type abstrait, une interface Ensemble, représentant une collection d’objets, qui sera implémentée par deux classes EnsembleTableau et EnsembleList à l’aide respectivement d’un tableau (Array) d’une liste (ArrayList).

Ce type doit permettre l’utilisation des opérations suivantes :
-	ajouter(), qui ajoute un élément à l’ensemble
-	estVide(), qui retourne vrai si l’ensemble l’est, faux sinon
-	element(), qui retourne l’élément dont l’index est passé en paramètre
-	appartient(), qui retourne vrai si l’élément passé en paramètre appartient à l’ensemble, faux sinon
-	taille(), qui retourne la taille de l’ensemble
-	tailleMax(), qui retourne la taille maximale de l’ensemble
-	copie(), qui retourne un ensemble avec les mêmes éléments
-	retournerEnlever(), qui retourne un élément de l’ensemble et qui le retire de celui-ci (attention aux cas de défense – vide, …)
-	fusionner(), qui fusionne l’ensemble avec un autre ensemble passé en paramètre tout en garantissant l’unicité des éléments

On imagine le type d’implémentation pour le type abstrait Pile, une interface qui proposera les méthodes suivantes :
-	ajouter(), qui ajoute un élément au sommet de la pile
-	estVide(), qui retourne vrai si la pile l’est, faux sinon
-	sommet(), qui lit le sommet de la pile sans retirer l’élément
-	depiler(), qui dépile l’élément au sommet de la pile et le retourne
-	taille(), qui retourne la taille de la pile
-	tailleMax(), qui retourne la taille maximale de la pile

Travail à faire :
•	Créer une archive (librairie) avec ces nouveaux types
•	Implémenter un programme qui permet de tester l’implémentation de cette nouvelle librairie

Reste à faire :
•	Compléter les méthodes implémentées avec des directives TODO
•	Proposer les implémentations pour la pile
•	On proposera ensuite une version générique de la libraire en utilisant le principe de "généricité"

NB : Prévoir une gestion d’exceptions dans le cas où les méthodes retournerEnlever, depiler et sommet  sont appelées sur des types vides.
